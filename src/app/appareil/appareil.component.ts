import { Component, Input, OnInit } from '@angular/core';
import { DeviceService } from '../services/device.service';

@Component({
  selector: 'app-appareil',
  templateUrl: './appareil.component.html',
  styleUrls: ['./appareil.component.scss']
})
export class AppareilComponent implements OnInit {

  @Input() deviceName: string;
  //private deviceName: string = 'Washing Machine';
  @Input() deviceStatus: string;
  //private status: string = 'off';
  @Input() deviceIndex: number;

  constructor(private deviceService: DeviceService) { }

  ngOnInit() {
  }

  private getStatus() {
    //return this.status;
  }

  private getColor() {
    if(this.deviceStatus === 'on') {
      return 'green';
    } else if (this.deviceStatus === 'off') {
      return 'red';
    }
  }

  private onSwitchOn() {
    this.deviceService.switchOn(this.deviceIndex);
  }

  private onSwitchOff() {
    this.deviceService.switchOff(this.deviceIndex);
  }


}
