export class DeviceService {

    devices = [
    { name: "Washing Machine", status: 'off'},
    { name: "Television", status: 'on'},
    { name: "Computer", status: 'on'}
    ];

    switchOnAll() {
        for(let devices of this.devices) {
            devices.status = 'on';
        }
    }

    switchOffAll() {
        for(let devices of this.devices) {
            devices.status = 'off';
        }
    }

    switchOn(index:number) {
        console.log(index);
        this.devices[index].status = 'on';
    }

    switchOff(index:number) {
        this.devices[index].status = 'off';
    }
}