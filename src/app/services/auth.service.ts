export class AuthService {

  isAuth: boolean = false;

  signIn() {

    return new Promise(
        (resolve, reject) => {
          this.isAuth = true;
          setTimeout(() => {
            resolve(true);
          }, 2000);
        }
      )

  }

  signOut() {
    this.isAuth = false;
  }

}