import { Component, OnInit } from '@angular/core';
import { DeviceService } from '../services/device.service';

@Component({
  selector: 'app-appareil-view',
  templateUrl: './appareil-view.component.html',
  styleUrls: ['./appareil-view.component.scss']
})
export class AppareilViewComponent implements OnInit {

  title = 'app';
  devices: any[];

  lastUpdate = new Promise(
    (resolve, reject) => {
      const date = new Date();
      setTimeout(() => {
        resolve(date);
      }, 5000);
    }
  )

  constructor(private deviceService: DeviceService) {
    setTimeout(() => {
      //this.isAuthenticated = true;
    }, 6000);
  }

  ngOnInit() {
    this.devices = this.deviceService.devices;
  }

  private setOn() {
    this.deviceService.switchOnAll();
  }

  private setOff() {
    this.deviceService.switchOffAll();
  }

}
